﻿using System;
using FIORest.Authentication;
using FIORest.Database;

using Microsoft.EntityFrameworkCore.Storage;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

namespace FIORest
{
    public class FIORequest<JsonRepr> : IDisposable
    {
        private Exception _exception = null;
        private IDbContextTransaction _transaction = null;

        public Request Request
        {
            get;
            private set;
        } = null;

        public string UserName
        {
            get; 
            private set;
        } = null;

        public DateTime Now
        {
            get
            {
                return DateTime.UtcNow;
            }
        }

        public RequestStream RequestStream
        {
            get;
            private set;
        } = null;

        public JsonRepr JsonPayload
        {
            get; private set;
        } = default;

        public PRUNDataContext DB
        {
            get; private set;
        } = null;

        public bool BadRequest
        {
            get
            {
                return (DB == null);
            }
        }

        public FIORequest(Request request)
        {
            Request = request;
            UserName = request.GetUserName();

            RequestStream = RequestStream.FromStream(request.Body);
            string requestBody = RequestStream.AsString();
            try
            {
                JsonPayload = JsonConvert.DeserializeObject<JsonRepr>(requestBody);
                DB = new PRUNDataContext();
                _transaction = DB.Database.BeginTransaction();
            }
            catch(Exception ex)
            {
                JsonPayload = default(JsonRepr);
                _exception = ex;
            }
        }

        public Response ReturnBadRequest()
        {
            return Utils.ReturnBadResponse(Request, _exception);
        }

        public void Dispose()
        {
            _transaction?.Commit();
            _transaction?.Dispose();
            _transaction = null;

            DB?.Dispose();
            DB = null;

            RequestStream?.Dispose();
            RequestStream = null;
        }
    }
}
