﻿namespace FIORest.JSONRepresentations.CompanyData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string name { get; set; }
        public string countryId { get; set; }
        public Owncurrency ownCurrency { get; set; }
        public Currencyaccount[] currencyAccounts { get; set; }
        public string startingProfile { get; set; }
        public Startinglocation startingLocation { get; set; }
        public Ratingreport ratingReport { get; set; }
    }

    public class Owncurrency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Startinglocation
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Ratingreport
    {
        public string overallRating { get; set; }
        public Subrating[] subRatings { get; set; }
    }

    public class Subrating
    {
        public string score { get; set; }
        public string rating { get; set; }
    }

    public class Currencyaccount
    {
        public string category { get; set; }
        public int type { get; set; }
        public int number { get; set; }
        public Bookbalance bookBalance { get; set; }
        public Currencybalance currencyBalance { get; set; }
    }

    public class Bookbalance
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Currencybalance
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }
}
