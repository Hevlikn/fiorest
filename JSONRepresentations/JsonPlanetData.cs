﻿namespace FIORest.JSONRepresentations.PlanetData
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string planetId { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public Namer namer { get; set; }
        public Namingdate namingDate { get; set; }
        public bool nameable { get; set; }
        public object[] celestialBodies { get; set; }
        public Address address { get; set; }
        public Data data { get; set; }
        public Buildoptions buildOptions { get; set; }
        public Project[] projects { get; set; }
        public Country country { get; set; }
        public Governor governor { get; set; }
        public Governingentity governingEntity { get; set; }
        public Currency currency { get; set; }
        public Localrules localRules { get; set; }
        public string populationId { get; set; }
        public string cogcId { get; set; }
        public string id { get; set; }
    }

    public class Namer
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Namingdate
    {
        public long timestamp { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Data
    {
        public float gravity { get; set; }
        public float magneticField { get; set; }
        public float mass { get; set; }
        public float massEarth { get; set; }
        public Orbit orbit { get; set; }
        public int orbitIndex { get; set; }
        public float pressure { get; set; }
        public float radiation { get; set; }
        public float radius { get; set; }
        public Resource[] resources { get; set; }
        public float sunlight { get; set; }
        public bool surface { get; set; }
        public float temperature { get; set; }
        public Plot[] plots { get; set; }
        public float fertility { get; set; }
    }

    public class Orbit
    {
        public long semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public int rightAscension { get; set; }
        public int periapsis { get; set; }
    }

    public class Resource
    {
        public string materialId { get; set; }
        public string type { get; set; }
        public float factor { get; set; }
    }

    public class Plot
    {
        public int index { get; set; }
        public int[] neighbours { get; set; }
        public Center center { get; set; }
    }

    public class Center
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class Buildoptions
    {
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string siteType { get; set; }
        public Billofmaterial billOfMaterial { get; set; }
    }

    public class Billofmaterial
    {
        public Quantity[] quantities { get; set; }
    }

    public class Quantity
    {
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Country
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Governor
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Governingentity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Currency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Localrules
    {
        public Currency1 currency { get; set; }
        public Collector collector { get; set; }
        public string[] governingEntityTypes { get; set; }
        public Productionfees productionFees { get; set; }
        public Localmarketfee localMarketFee { get; set; }
        public Warehousefee warehouseFee { get; set; }
    }

    public class Currency1
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Collector
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Productionfees
    {
        public Fee[] fees { get; set; }
    }

    public class Fee
    {
        public string category { get; set; }
        public Fee1 fee { get; set; }
    }

    public class Fee1
    {
        public int amount { get; set; }
        public string currency { get; set; }
    }

    public class Localmarketfee
    {
        public int _base { get; set; }
        public int timeFactor { get; set; }
    }

    public class Warehousefee
    {
        public int fee { get; set; }
    }

    public class Project
    {
        public string type { get; set; }
        public string entityId { get; set; }
    }

}
