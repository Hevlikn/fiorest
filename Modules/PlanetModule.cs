﻿using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class PlanetModule : NancyModule
    {
        public PlanetModule() : base("/planet")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostPlanet();
            });

            Post("/search", _ =>
            {
                this.EnforceAuth();
                return SearchPlanet();
            });

            Get("/{planet}", parameters =>
            {
                return GetPlanet(parameters.planet);
            });
        }

        private Response PostPlanet()
        {
            using (var req = new FIORequest<JSONRepresentations.PlanetData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;
                PlanetDataModel model = req.DB.PlanetDataModels.Where(p => p.NaturalId == data.naturalId).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new PlanetDataModel();
                }
                else
                {
                    model.Resources.Clear();
                    model.BuildRequirements.Clear();
                    model.ProductionFees.Clear();
                }

                model.Id = data.id;
                model.NaturalId = data.naturalId;
                model.Name = data.name;
                model.Namer = (data.namer != null) ? data.namer.username.ToString() : null;
                model.NamingDataEpochMs = (data.namingDate != null) ? data.namingDate.timestamp : 0;
                model.Nameable = data.nameable;

                model.Gravity = data.data.gravity;
                model.MagneticField = data.data.magneticField;
                model.Mass = data.data.mass;
                model.MassEarth = data.data.massEarth;

                model.OrbitSemiMajorAxis = data.data.orbit.semiMajorAxis;
                model.OrbitEccentricity = data.data.orbit.eccentricity;
                model.OrbitInclination = data.data.orbit.inclination;
                model.OrbitRightAscension = data.data.orbit.rightAscension;
                model.OrbitPeriapsis = data.data.orbit.periapsis;
                model.OrbitIndex = data.data.orbitIndex;

                model.Pressure = data.data.pressure;
                model.Radiation = data.data.radiation;
                model.Radius = data.data.radius;

                foreach (var resource in data.data.resources)
                {
                    PlanetDataResource planetResource = new PlanetDataResource();

                    planetResource.MaterialId = resource.materialId;
                    planetResource.ResourceType = resource.type;
                    planetResource.Factor = resource.factor;

                    model.Resources.Add(planetResource);
                }

                model.Sunlight = data.data.sunlight;
                model.Surface = data.data.surface;
                model.Temperature = data.data.temperature;
                model.Fertility = data.data.fertility;

                foreach (var option in data.buildOptions.options)
                {
                    if (option.siteType == "PLAYER_SITE")
                    {
                        foreach (var quantity in option.billOfMaterial.quantities)
                        {
                            PlanetBuildRequirement requirement = new PlanetBuildRequirement();

                            requirement.MaterialName = quantity.material.name;
                            requirement.MaterialId = quantity.material.id;
                            requirement.MaterialTicker = quantity.material.ticker;
                            requirement.MaterialCategory = quantity.material.category;

                            requirement.MaterialAmount = quantity.amount;

                            requirement.MaterialWeight = quantity.material.weight;
                            requirement.MaterialVolume = quantity.material.volume;

                            model.BuildRequirements.Add(requirement);
                        }
                    }
                }

                model.HasLocalMarket = false;
                model.HasChamberOfCommerce = false;
                model.HasWarehouse = false;
                model.HasAdministrationCenter = false;
                foreach (var project in data.projects)
                {
                    switch (project.type)
                    {
                        case "LOCM":
                            model.HasLocalMarket = true;
                            break;

                        case "COGC":
                            model.HasChamberOfCommerce = true;
                            break;

                        case "WAR":
                            model.HasWarehouse = true;
                            break;

                        case "ADM":
                            model.HasAdministrationCenter = true;
                            break;
                    }
                }

                model.FactionCode = data.country?.code;
                model.FactionName = data.country?.name;

                model.GovernorId = data.governor?.id;
                model.GovernorUserName = data.governor?.username;

                model.GovernorCorporationId = data.governingEntity?.id;
                model.GovernorCorporationName = data.governingEntity?.name;
                model.GovernorCorporationCode = data.governingEntity?.code;

                model.CurrencyName = data.currency?.name;
                model.CurrencyCode = data.currency?.code;

                model.CollectorId = data.localRules?.collector?.id;
                model.CollectorName = data.localRules?.collector?.name;
                model.CollectorCode = data.localRules?.collector?.code;

                if (data.localRules?.productionFees?.fees != null)
                {
                    foreach (var fee in data.localRules.productionFees.fees)
                    {
                        PlanetProductionFee productionFee = new PlanetProductionFee();

                        productionFee.Category = fee.category;
                        productionFee.FeeAmount = fee.fee.amount;
                        productionFee.FeeCurrency = fee.fee.currency;

                        model.ProductionFees.Add(productionFee);
                    }
                }

                model.BaseLocalMarketFee = data.localRules?.localMarketFee?._base;
                model.LocalMarketFeeFactor = data.localRules?.localMarketFee?.timeFactor;

                model.WarehouseFee = data.localRules?.warehouseFee?.fee;

                model.PopulationId = data.populationId;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.PlanetDataModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response SearchPlanet()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonPlanetSearch>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var queryData = req.JsonPayload;

                // Make sure there's at least one material requested
                if ( queryData.Materials.Count == 0 && !queryData.MustBeFertile )
                {
                    Response resp = "{ \"ErrorMessage\": \"Please specify at least one material or enforce fertility.\" }";
                    resp.StatusCode = HttpStatusCode.BadRequest;
                    return resp;
                }

                // Search the materials first
                List<string> materialIds = new List<string>();
                foreach(var material in queryData.Materials)
                {
                    var matModel = req.DB.MATModels.Where(m => m.Id == material || m.Name == material || m.Ticker == material).FirstOrDefault();
                    if (matModel != null)
                    {
                        materialIds.Add(matModel.Id);
                    }
                }

                IQueryable<PlanetDataModel> res = req.DB.PlanetDataModels;
                foreach (var materialId in materialIds)
                {
                    res = from r in res from resource in r.Resources where materialId == resource.MaterialId select r;
                }

                if (!queryData.IncludeRocky)
                {
                    res = res.Where(pdm => pdm.Surface == false);
                }
                if (!queryData.IncludeGaseous)
                {
                    res = res.Where(pdm => pdm.Surface == true);
                }
                if (!queryData.IncludeLowGravity)
                {
                    res = res.Where(pdm => pdm.Gravity >= 0.25);
                }
                if (!queryData.IncludeHighGravity)
                {
                    res = res.Where(pdm => pdm.Gravity <= 2.5);
                }
                if (!queryData.IncludeLowPressure)
                {
                    res = res.Where(pdm => pdm.Pressure >= 0.25);
                }
                if (!queryData.IncludeHighPressure)
                {
                    res = res.Where(pdm => pdm.Pressure <= 2.0);
                }
                if (!queryData.IncludeLowTemperature)
                {
                    res = res.Where(pdm => pdm.Temperature >= -25.0);
                }
                if (!queryData.IncludeHighTemperature)
                {
                    res = res.Where(pdm => pdm.Temperature <= 75.0);
                }

                if (queryData.MustBeFertile)
                {
                    res = res.Where(pdm => pdm.Fertility > -1.0);
                }
                if (queryData.MustHaveLocalMarket)
                {
                    res = res.Where(pdm => pdm.HasLocalMarket);
                }
                if (queryData.MustHaveChamberOfCommerce)
                {
                    res = res.Where(pdm => pdm.HasChamberOfCommerce);
                }
                if (queryData.MustHaveWarehouse)
                {
                    res = res.Where(pdm => pdm.HasWarehouse);
                }
                if (queryData.MustHaveAdministrationCenter)
                {
                    res = res.Where(pdm => pdm.HasAdministrationCenter);
                }
                
                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        private Response GetPlanet(string Planet)
        {
            Planet = Planet.ToUpper();

            using (var DB = new PRUNDataContext())
            {
                var res = DB.PlanetDataModels.Where(p => p.Id.ToUpper() == Planet || p.NaturalId.ToUpper() == Planet || p.Name == Planet).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}
