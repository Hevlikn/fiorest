﻿using System.Linq;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Nancy;

namespace FIORest.Modules
{
    public class CreatePayload
    {
        public string UserName;
        public string Password;
        public bool IsAdmin;
    }

    public class DisablePayload
    {
        public string UserName;
        public string Reason;
    }

    public class AdminModule : NancyModule
    {
        public AdminModule() : base("/admin")
        {
            this.EnforceAuthAdmin();

            Get("/", _ =>
            {
                return HttpStatusCode.OK;
            });

            Get("/{username}", parameters =>
            {
                return UserNameExists(parameters.username);
            });

            Post("/create", _ =>
            {
                return Create();
            });

            Post("/disable", _ =>
            {
                return Disable();
            });

            Post("/clearcxdata", _ =>
            {
                return PostClearCXData();
            });

            Post("/resetuserdata/{username}", parameters =>
            {
                return PostResetUserData(parameters.username);
            });
        }

        private Response UserNameExists(string UserName)
        {
            using (var DB = new PRUNDataContext())
            {
                var authModel = DB.AuthenticationModels.Where(a => a.UserName.ToUpper() == UserName.ToUpper()).FirstOrDefault();
                if (authModel != null)
                {
                    return HttpStatusCode.OK;
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response Create()
        {
            using (var req = new FIORequest<CreatePayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var createPayload = req.JsonPayload;
                var authModel = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == createPayload.UserName.ToUpper()).FirstOrDefault();
                bool bShouldAdd = (authModel == null);
                if (bShouldAdd)
                {
                    authModel = new AuthenticationModel();
                }

                authModel.AccountEnabled = true;
                authModel.UserName = createPayload.UserName.ToUpper();
                authModel.Password = createPayload.Password;
                authModel.IsAdministrator = createPayload.IsAdmin;

                if (bShouldAdd)
                {
                    req.DB.AuthenticationModels.Add(authModel);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        public Response Disable()
        {
            using (var req = new FIORequest<DisablePayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var disablePayload = req.JsonPayload;
                var authModel = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == disablePayload.UserName.ToUpper()).FirstOrDefault();
                if (authModel != null)
                {
                    authModel.AccountEnabled = false;
                    authModel.DisabledReason = disablePayload.Reason;
                    req.DB.SaveChanges();
                    return HttpStatusCode.OK;
                }

                return HttpStatusCode.BadRequest;
            }
        }

        private Response PostClearCXData()
        {
            using (var DB = new PRUNDataContext())
            using(var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.CXDataModels select c;
                DB.CXDataModels.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostResetUserData(string UserName)
        {
            UserName = UserName.ToUpper();

            using (var DB = new PRUNDataContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.CompanyDataModels.RemoveRange(DB.CompanyDataModels.Where(c => c.UserNameSubmitted.ToUpper() == UserName));
                DB.PRODLinesModels.RemoveRange(DB.PRODLinesModels.Where(p => p.UserNameSubmitted.ToUpper() == UserName));
                DB.SHIPSModels.RemoveRange(DB.SHIPSModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.SITESModels.RemoveRange(DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.StorageModels.RemoveRange(DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.WorkforceModels.RemoveRange(DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }
    }   
}
