﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class InfrastructureModule : NancyModule
    {
        public InfrastructureModule() : base("/infrastructure")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostInfrastructure();
            });

            Get("/{planet_or_infrastructure_id}", parameters =>
            {
                return GetInfrastructure(parameters.planet_or_infrastructure_id);
            });
        }

        private Response PostInfrastructure()
        {
            using (var req = new FIORequest<JSONRepresentations.Infrastructure.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;
                var model = req.DB.InfrastructureModels.Where(i => i.PopulationId == data.id).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new InfrastructureModel();
                }
                else
                {
                    model.InfrastructureInfos.Clear();
                }

                model.PopulationId = data.id;

                foreach (var infrastructure in data.infrastructure)
                {
                    InfrastructureInfo infInfo = new InfrastructureInfo();

                    infInfo.Type = infrastructure.type;
                    infInfo.Ticker = infrastructure.ticker;
                    infInfo.Name = infrastructure.projectName;
                    infInfo.Level = infrastructure.level;
                    infInfo.ActiveLevel = infrastructure.activeLevel;
                    infInfo.CurrentLevel = infrastructure.currentLevel;
                    infInfo.UpkeepStatus = infrastructure.upkeepStatus;
                    infInfo.UpgradeStatus = infrastructure.upgradeStatus;

                    model.InfrastructureInfos.Add(infInfo);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.InfrastructureModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetInfrastructure(string PlanetOrInfrastructureId)
        {
            PlanetOrInfrastructureId = PlanetOrInfrastructureId.ToUpper();

            using (var DB = new PRUNDataContext())
            {
                var model = DB.InfrastructureModels.Where(i => i.PopulationId.ToUpper() == PlanetOrInfrastructureId).FirstOrDefault();
                if (model != null )
                {
                    return JsonConvert.SerializeObject(model);
                }

                var planet = DB.PlanetDataModels.Where(p => p.Id.ToUpper() == PlanetOrInfrastructureId || p.NaturalId.ToUpper() == PlanetOrInfrastructureId || p.Name.ToUpper() == PlanetOrInfrastructureId).FirstOrDefault();
                if ( planet != null )
                {
                    model = DB.InfrastructureModels.Where(i => i.PopulationId == planet.PopulationId).FirstOrDefault();
                    if ( model != null)
                    {
                        return JsonConvert.SerializeObject(model);
                    }
                }

                return HttpStatusCode.NoContent;
            }
        }
    }
}
