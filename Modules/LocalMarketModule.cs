﻿using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class LocalMarketModule : NancyModule
    {
        public LocalMarketModule() : base("/localmarket")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostLocalMarket();
            });

            Get("/{local_market_id}", parameters =>
            {
                return GetLocalMarket(parameters.local_market_id);
            });

            Get("/planet/{planet}", parameters =>
            {
                return GetAds(parameters.planet);
            });

            Get("/planet/{planet}/{type}", parameters =>
            {
                return GetAds(parameters.planet, parameters.type);
            });

            Get("/shipping/source/{source}", parameters =>
            {
                return GetAdsWithSource(parameters.source);
            });

            Get("/shipping/destination/{destination}", parameters =>
            {
                return GetAdsWithDestination(parameters.destination);
            });

            Get("/company/{company_name}", parameters =>
            {
                return GetAdsForCompany(parameters.company_name);
            });
        }

        private Response PostLocalMarket()
        {
            using (var req = new FIORequest<JSONRepresentations.LocalMarket.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                string MarketId = rootObj.payload.message.payload.path[1];

                var data = rootObj.payload.message.payload.body;
                var model = req.DB.LocalMarketModels.Where(lm => lm.MarketId == MarketId).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new LocalMarketModel();
                }
                else
                {
                    model.ShippingAds.Clear();
                    model.BuyingAds.Clear();
                    model.SellingAds.Clear();
                }

                model.MarketId = MarketId;

                foreach (var ad in data)
                {
                    switch (ad.type)
                    {
                        case "COMMODITY_SHIPPING":
                            {
                                ShippingAd shipAd = new ShippingAd();

                                shipAd.PlanetId = ad.address.lines[1].entity.id;
                                shipAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                shipAd.PlanetName = ad.address.lines[1].entity.name;

                                shipAd.OriginPlanetId = ad.origin.lines[1].entity.id;
                                shipAd.OriginPlanetNaturalId = ad.origin.lines[1].entity.naturalId;
                                shipAd.OriginPlanetName = ad.origin.lines[1].entity.name;

                                shipAd.DestinationPlanetId = ad.destination.lines[1].entity.id;
                                shipAd.DestinationPlanetNaturalId = ad.destination.lines[1].entity.naturalId;
                                shipAd.DestinationPlanetName = ad.destination.lines[1].entity.name;

                                shipAd.CargoWeight = ad.cargoWeight;
                                shipAd.CargoVolume = ad.cargoVolume;

                                shipAd.CreatorCompanyId = ad.creator.id;
                                shipAd.CreatorCompanyName = ad.creator.name;
                                shipAd.CreatorCompanyCode = ad.creator.code;

                                shipAd.PayoutPrice = ad.price.amount;
                                shipAd.PayoutCurrency = ad.price.currency;

                                shipAd.DeliveryTime = ad.advice;

                                shipAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                shipAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                shipAd.MinimumRating = ad.minimumRating;

                                model.ShippingAds.Add(shipAd);
                            }
                            break;
                        case "COMMODITY_BUYING":
                            {
                                BuyingAd buyAd = new BuyingAd();

                                buyAd.PlanetId = ad.address.lines[1].entity.id;
                                buyAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                buyAd.PlanetName = ad.address.lines[1].entity.name;

                                buyAd.CreatorCompanyId = ad.creator.id;
                                buyAd.CreatorCompanyName = ad.creator.name;
                                buyAd.CreatorCompanyCode = ad.creator.code;

                                buyAd.MaterialId = ad.quantity.material.id;
                                buyAd.MaterialName = ad.quantity.material.name;
                                buyAd.MaterialTicker = ad.quantity.material.ticker;
                                buyAd.MaterialCategory = ad.quantity.material.category;
                                buyAd.MaterialWeight = ad.quantity.material.weight;
                                buyAd.MaterialVolume = ad.quantity.material.volume;

                                buyAd.MaterialAmount = ad.quantity.amount;

                                buyAd.Price = ad.price.amount;
                                buyAd.PriceCurrency = ad.price.currency;

                                buyAd.DeliveryTime = ad.advice;

                                buyAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                buyAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                buyAd.MinimumRating = ad.minimumRating;

                                model.BuyingAds.Add(buyAd);
                            }
                            break;
                        case "COMMODITY_SELLING":
                            {
                                SellingAd sellAd = new SellingAd();

                                sellAd.PlanetId = ad.address.lines[1].entity.id;
                                sellAd.PlanetNaturalId = ad.address.lines[1].entity.naturalId;
                                sellAd.PlanetName = ad.address.lines[1].entity.name;

                                sellAd.CreatorCompanyId = ad.creator.id;
                                sellAd.CreatorCompanyName = ad.creator.name;
                                sellAd.CreatorCompanyCode = ad.creator.code;

                                sellAd.MaterialId = ad.quantity.material.id;
                                sellAd.MaterialName = ad.quantity.material.name;
                                sellAd.MaterialTicker = ad.quantity.material.ticker;
                                sellAd.MaterialCategory = ad.quantity.material.category;
                                sellAd.MaterialWeight = ad.quantity.material.weight;
                                sellAd.MaterialVolume = ad.quantity.material.volume;

                                sellAd.MaterialAmount = ad.quantity.amount;

                                sellAd.Price = ad.price.amount;
                                sellAd.PriceCurrency = ad.price.currency;

                                sellAd.DeliveryTime = ad.advice;

                                sellAd.CreationTimeEpochMs = ad.creationTime.timestamp;
                                sellAd.ExpiryTimeEpochMs = ad.expiry.timestamp;

                                sellAd.MinimumRating = ad.minimumRating;

                                model.SellingAds.Add(sellAd);
                            }
                            break;
                    }
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.LocalMarketModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetLocalMarket(string MarketId)
        {
            using (var DB = new PRUNDataContext())
            {
                var model = DB.LocalMarketModels.Where(lm => lm.MarketId == MarketId).FirstOrDefault();
                if (model != null )
                {
                    return JsonConvert.SerializeObject(model);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        protected class AdsResult
        {
            public List<ShippingAd> ShippingAds;
            public List<BuyingAd> BuyingAds;
            public List<SellingAd> SellingAds;
        }


        private Response GetAds(string Planet)
        {
            Planet = Planet.ToUpper();

            using (var DB = new PRUNDataContext())
            {
                AdsResult res = new AdsResult();
                res.ShippingAds = DB.ShippingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                res.BuyingAds = DB.BuyingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                res.SellingAds = DB.SellingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                if (res.ShippingAds.Count > 0 || res.BuyingAds.Count > 0 || res.SellingAds.Count > 0 )
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetAds(string Planet, string Type)
        {
            Planet = Planet.ToUpper();
            Type = Type.ToUpper();

            using (var DB = new PRUNDataContext())
            {
                switch (Type)
                {
                    case "BUY":
                    case "BUYS":
                    case "BUYING":
                        {
                            var res = DB.BuyingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                    case "SELL":
                    case "SELLS":
                    case "SELLING":
                        {
                            var res = DB.SellingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                    case "SHIP":
                    case "SHIPPING":
                        {
                            var res = DB.ShippingAds.Where(a => a.PlanetId.ToUpper() == Planet || a.PlanetNaturalId.ToUpper() == Planet || a.PlanetName.ToUpper() == Planet).ToList();
                            if (res.Count > 0)
                            {
                                return JsonConvert.SerializeObject(res);
                            }
                        }
                        break;
                }

                return HttpStatusCode.NoContent;
            }
        }

        public Response GetAdsWithSource(string SourcePlanet)
        {
            SourcePlanet = SourcePlanet.ToUpper();
            using(var DB = new PRUNDataContext())
            {
                var res = DB.ShippingAds.Where(a => a.OriginPlanetId.ToUpper() == SourcePlanet || a.OriginPlanetName.ToUpper() == SourcePlanet || a.OriginPlanetNaturalId.ToUpper() == SourcePlanet).ToList();
                if ( res.Count > 0)
                {
                    return JsonConvert.SerializeObject(res);
                }

                return HttpStatusCode.NoContent;
            }
        }

        public Response GetAdsWithDestination(string DestinationPlanet)
        {
            DestinationPlanet = DestinationPlanet.ToUpper();
            using (var DB = new PRUNDataContext())
            {
                var res = DB.ShippingAds.Where(a => a.DestinationPlanetId.ToUpper() == DestinationPlanet || a.DestinationPlanetName.ToUpper() == DestinationPlanet || a.DestinationPlanetNaturalId.ToUpper() == DestinationPlanet).ToList();
                if (res.Count > 0)
                {
                    return JsonConvert.SerializeObject(res);
                }

                return HttpStatusCode.NoContent;
            }
        }

        class AllCompanyAds
        {
            public List<BuyingAd> BuyingAds { get; set; } = new List<BuyingAd>();
            public List<SellingAd> SellingAds { get; set; } = new List<SellingAd>();
            public List<ShippingAd> ShippingAds { get; set; } = new List<ShippingAd>();
        }

        public Response GetAdsForCompany(string CompanyName)
        {
            CompanyName = CompanyName.ToUpper();
            using (var DB = new PRUNDataContext())
            {
                var buyAds = DB.BuyingAds.Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName).ToList();
                var sellAds = DB.SellingAds.Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName).ToList();
                var shippingAds = DB.ShippingAds.Where(a => a.CreatorCompanyCode.ToUpper() == CompanyName || a.CreatorCompanyId.ToUpper() == CompanyName || a.CreatorCompanyName.ToUpper() == CompanyName).ToList();
                if (buyAds.Count > 0 || sellAds.Count > 0 || shippingAds.Count > 0)
                {
                    AllCompanyAds all = new AllCompanyAds();
                    all.BuyingAds = buyAds;
                    all.SellingAds = sellAds;
                    all.ShippingAds = shippingAds;
                    return JsonConvert.SerializeObject(all);
                }

                return HttpStatusCode.NoContent;
            }
        }
    }
}
