﻿using Nancy;

namespace FIORest
{
    public class RootModule : NancyModule
    {
        public RootModule()
        {
            Get("/", _ =>
            {
                return "FIO REST Endpoint";
            });
        }
    }
}
