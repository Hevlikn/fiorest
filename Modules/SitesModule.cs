﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class SitesModule : NancyModule
    {
        public SitesModule() : base("/sites")
        {
            this.EnforceAuth();

            Post("/", _ =>
            {
                return PostSites();
            });

            Get("/{username}", parameters =>
            {
                return GetSites(parameters.username);
            });

            Get("/{username}/{planet}", parameters =>
            {
                return GetSite(parameters.username, parameters.planet);
            });

            Get("/planets/{username}", parameters =>
            {
                return GetPlanetSites(parameters.username);
            });
        }

        private Response PostSites()
        {
            using (var req = new FIORequest<JSONRepresentations.SiteSites.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                SITESModel model = req.DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == req.UserName).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new SITESModel();
                }
                else
                {
                    model.Sites.Clear();
                }

                var data = rootObj.payload.message.payload;

                foreach (var site in data.sites)
                {
                    SITESSite sitesSite = new SITESSite();

                    sitesSite.SiteId = site.siteId;
                    foreach (var line in site.address.lines)
                    {
                        if (line.type == "PLANET")
                        {
                            sitesSite.PlanetId = line.entity.id;
                            sitesSite.PlanetIdentifier = line.entity.naturalId;
                            sitesSite.PlanetName = line.entity.name;
                            break;
                        }
                    }
                    sitesSite.PlanetFoundedEpochMs = site.founded.timestamp;

                    foreach (var platform in site.platforms)
                    {
                        SITESBuilding sitesBuilding = new SITESBuilding();

                        sitesBuilding.BuildingName = platform.module.reactorName;
                        sitesBuilding.BuildingTicker = platform.module.reactorTicker;
                        sitesBuilding.Condition = platform.condition;

                        foreach (var reclaimable in platform.reclaimableMaterials)
                        {
                            SITESReclaimableMaterial sitesReclaimable = new SITESReclaimableMaterial();

                            sitesReclaimable.MaterialId = reclaimable.material.id;
                            sitesReclaimable.MaterialTicker = reclaimable.material.ticker;
                            sitesReclaimable.MaterialAmount = reclaimable.amount;
                            sitesReclaimable.MaterialName = reclaimable.material.name;

                            sitesBuilding.ReclaimableMaterials.Add(sitesReclaimable);
                        }

                        foreach (var repairable in platform.repairMaterials)
                        {
                            SITESRepairMaterial sitesRepairable = new SITESRepairMaterial();

                            sitesRepairable.MaterialId = repairable.material.id;
                            sitesRepairable.MaterialTicker = repairable.material.ticker;
                            sitesRepairable.MaterialAmount = repairable.amount;
                            sitesRepairable.MaterialName = repairable.material.name;

                            sitesBuilding.RepairMaterials.Add(sitesRepairable);
                        }

                        sitesSite.Buildings.Add(sitesBuilding);
                    }

                    model.Sites.Add(sitesSite);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.SITESModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = new PRUNDataContext())
                {
                    var res = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetSite(string UserName, string Planet)
        {
            UserName = UserName.ToUpper();
            Planet = Planet.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = new PRUNDataContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var site = sites.Sites.Where(s => s.PlanetId.ToUpper() == Planet || s.PlanetIdentifier.ToUpper() == Planet || s.PlanetName.ToUpper() == Planet).FirstOrDefault();
                        if ( site != null)
                        {
                            return JsonConvert.SerializeObject(site);
                        }
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetPlanetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = new PRUNDataContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var planets = sites.Sites.Select(s => s.PlanetId).ToList();
                        return JsonConvert.SerializeObject(planets);
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
