﻿using System;
using System.Collections.Generic;
using System.Linq;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Nancy;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class LoginPayload
    {
        public string UserName;
        public string Password;
    }

    public class LoginResponsePayload
    {
        public string AuthToken;
        public DateTime Expiry;
    }

    public class ChangePasswordPayload
    {
        public string OldPassword;
        public string NewPassword;
    }

    public class AuthModule : NancyModule
    {
        public AuthModule() : base("/auth")
        {
            Get("/", parameters =>
            {
                return Request.IsAuthenticated() ? HttpStatusCode.OK : HttpStatusCode.Unauthorized;
            });

            Post("/login", _ =>
            {
                return PostLogin();
            });

            Post("/refreshauthtoken", _ =>
            {
                return PostRefreshAuthToken();
            });

            Post("/changepassword", _ =>
            {
                return PostChangePassword();
            });

            Post("/addpermission", _ =>
            {
                return PostAddPermission();
            });

            Post("/deletepermission/{username}", parameters =>
            {
                return PostDeletePermission(parameters.username);
            });

            Get("/permissions", _ =>
            {
                return GetPermissions();
            });

            Get("/visibility", _ =>
            {
                return GetVisibility();
            });

            Get("/visibility/{permissiontype}", parameters =>
            {
                return GetVisibility(parameters.permissiontype);
            });
        }

        private Response PostLogin()
        {
            using (var req = new FIORequest<LoginPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    // First check for failed attempts brute force
                    DateTime oneHourAgo = req.Now.AddHours(-1);
                    var failedAttempts = result.FailedAttempts.Where(f => f.FailedAttemptDateTime > oneHourAgo && f.Address == req.Request.UserHostAddress).ToList();
                    if (failedAttempts.Count > 5)
                    {
                        Response resp = "Too many failed attempts. Locked out.";
                        resp.StatusCode = HttpStatusCode.Unauthorized;
                        return resp;
                    }

                    if (result.Password == req.JsonPayload.Password)
                    {
                        if (req.Now < result.AuthorizationExpiry)
                        {
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }
                        else
                        {
                            result.AuthorizationKey = Guid.NewGuid();
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }

                        LoginResponsePayload response = new LoginResponsePayload();
                        response.AuthToken = result.AuthorizationKey.ToString("N");
                        response.Expiry = result.AuthorizationExpiry;

                        req.DB.SaveChanges();

                        Response resp = JsonConvert.SerializeObject(response);
                        resp.ContentType = "application/json";
                        resp.Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } };
                        return resp;
                    }
                    else
                    {
                        result.FailedAttempts.Add(new FailedLoginAttempt { FailedAttemptDateTime = req.Now, Address = req.Request.UserHostAddress });
                        return HttpStatusCode.Unauthorized;
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response PostRefreshAuthToken()
        {
            if(Request.IsAuthenticated())
            {
                using (var DB = new PRUNDataContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var result = DB.AuthenticationModels.Where(e => e.AuthorizationKey.ToString() == Request.GetAuthToken()).FirstOrDefault();
                    if ( result != null )
                    {
                        result.AuthorizationExpiry = DateTime.UtcNow + new TimeSpan(1, 0, 0, 0);
                        DB.SaveChanges();
                        transaction.Commit();

                        return HttpStatusCode.OK;
                    }
                }

                return HttpStatusCode.BadRequest;
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostChangePassword()
        {
            if (Request.IsAuthenticated())
            {
                using (var req = new FIORequest<ChangePasswordPayload>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var result = req.DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == req.UserName.ToUpper() && e.Password == req.JsonPayload.OldPassword);
                    if (result != null)
                    {
                        result.Password = req.JsonPayload.NewPassword;
                        req.DB.SaveChanges();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.Unauthorized;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostAddPermission()
        {
            if (Request.IsAuthenticated())
            {
                using (var req = new FIORequest<PermissionAllowance>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var setPermissionPayload = req.JsonPayload;

                    bool UserInPayloadExists = (req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault() != null);
                    if (setPermissionPayload.UserName == "*" || UserInPayloadExists)
                    {
                        string UserName = Request.GetUserName();
                        var result = req.DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName);
                        if (result != null)
                        {
                            PermissionAllowance existingPermission = result.Allowances.Where(a => a.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault();
                            if (existingPermission != null)
                            {
                                existingPermission.FlightData = setPermissionPayload.FlightData;
                                existingPermission.BuildingData = setPermissionPayload.BuildingData;
                                existingPermission.StorageData = setPermissionPayload.StorageData;
                                existingPermission.ProductionData = setPermissionPayload.ProductionData;
                                existingPermission.WorkforceData = setPermissionPayload.WorkforceData;
                                existingPermission.ExpertsData = setPermissionPayload.ExpertsData;
                            }
                            else
                            {
                                result.Allowances.Add(setPermissionPayload);
                            }
                            req.DB.SaveChanges();
                            return HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostDeletePermission(string PermUserName)
        {
            PermUserName = PermUserName.ToUpper();
            if (Request.IsAuthenticated())
            {
                string UserName = Request.GetUserName();

                using (var DB = new PRUNDataContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var AuthModelId = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName).AuthenticationModelId;
                    var Allowance = DB.PermissionAllowances.Where(p => p.UserName.ToUpper() == PermUserName && p.AuthenticationModelId == AuthModelId).FirstOrDefault();
                    if (Allowance != null)
                    {
                        DB.PermissionAllowances.Attach(Allowance);
                        DB.PermissionAllowances.Remove(Allowance);
                        DB.SaveChanges();
                        transaction.Commit();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetPermissions()
        {
            if (Request.IsAuthenticated())
            {
                string UserName = Request.GetUserName();
                using (var DB = new PRUNDataContext())
                {
                    return JsonConvert.SerializeObject(DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName).Allowances);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class Visibility
        {
            public string PermissionType { get; set; }
            public List<string> UserNames { get; set; } = new List<string>();     
        }

        private Response GetVisibility(string permissionType = null)
        {
            if (Request.IsAuthenticated())
            {
                string UserName = Request.GetUserName();
                using (var DB = new PRUNDataContext())
                {
                    if( permissionType != null)
                    {
                        List<string> UserNames = null;
                        switch (permissionType.ToUpper())
                        {
                            case "FLIGHT":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.FlightData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "BUILDING":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.BuildingData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "STORAGE":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.StorageData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "PRODUCTION":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ProductionData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "WORKFORCE":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.WorkforceData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "EXPERTS":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ExpertsData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            default:
                                return HttpStatusCode.BadRequest;
                        }

                        // Remove ourselves
                        UserNames.Remove(UserName);

                        return JsonConvert.SerializeObject(UserNames);
                    }
                    else
                    {
                        List<PermissionAllowance> allAllowances = DB.AuthenticationModels
                            .SelectMany(auth => auth.Allowances, (auth, allowances) => new {auth, allowances})
                            .Where(authAndAllowances => (authAndAllowances.allowances.UserName.ToUpper() == UserName || authAndAllowances.allowances.UserName == "*") && authAndAllowances.auth.UserName.ToUpper() != UserName)
                            .Select(authAndAllowances =>
                            new PermissionAllowance
                            {
                                UserName = authAndAllowances.auth.UserName,
                                FlightData = authAndAllowances.allowances.FlightData,
                                BuildingData = authAndAllowances.allowances.BuildingData,
                                StorageData = authAndAllowances.allowances.StorageData,
                                ProductionData = authAndAllowances.allowances.ProductionData,
                                WorkforceData = authAndAllowances.allowances.WorkforceData,
                                ExpertsData = authAndAllowances.allowances.ExpertsData
                            }).ToList();

                        return JsonConvert.SerializeObject(allAllowances);
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
