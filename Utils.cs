﻿using System;
using System.Collections.Generic;

using FIORest.Authentication;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

namespace FIORest
{
    public static class Utils
    {
        public static Response ReturnBadResponse(Request request, Exception ex)
        {
            string UserName = request.GetUserName();

            using (var requestStream = RequestStream.FromStream(request.Body))
            {
                string requestBody = requestStream.AsString();

                var error = new Dictionary<string, string>
                {
                    {"Type", ex.GetType().ToString()},
                    {"Message", ex.Message},
                    {"StackTrace", ex.StackTrace}
                };

                Logger.LogBadRequest(requestBody, UserName, error);

#if DEBUG
                // Only return the payload if we're debug
                string errorJson = JsonConvert.SerializeObject(error);
                Response resp = errorJson;
                resp.StatusCode = HttpStatusCode.BadRequest;
                resp.ContentType = "application/json";
                return resp;
#else
                return HttpStatusCode.BadRequest;
#endif
            }
        }
    }
}
