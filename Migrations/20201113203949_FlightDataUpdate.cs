﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class FlightDataUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegmentDestinations");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegmentOrigins");

            migrationBuilder.RenameColumn(
                name: "OriginName",
                table: "FLIGHTSFlightSegments",
                newName: "Origin");

            migrationBuilder.RenameColumn(
                name: "OriginId",
                table: "FLIGHTSFlightSegments",
                newName: "Destination");

            migrationBuilder.AddColumn<double>(
                name: "FtlDistance",
                table: "FLIGHTSFlightSegments",
                type: "REAL",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FtlFuelConsumption",
                table: "FLIGHTSFlightSegments",
                type: "REAL",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "StlDistance",
                table: "FLIGHTSFlightSegments",
                type: "REAL",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "StlFuelConsumption",
                table: "FLIGHTSFlightSegments",
                type: "REAL",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrentSegmentIndex",
                table: "FLIGHTSFlights",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "FtlDistance",
                table: "FLIGHTSFlights",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "IsAborted",
                table: "FLIGHTSFlights",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "StlDistance",
                table: "FLIGHTSFlights",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FtlDistance",
                table: "FLIGHTSFlightSegments");

            migrationBuilder.DropColumn(
                name: "FtlFuelConsumption",
                table: "FLIGHTSFlightSegments");

            migrationBuilder.DropColumn(
                name: "StlDistance",
                table: "FLIGHTSFlightSegments");

            migrationBuilder.DropColumn(
                name: "StlFuelConsumption",
                table: "FLIGHTSFlightSegments");

            migrationBuilder.DropColumn(
                name: "CurrentSegmentIndex",
                table: "FLIGHTSFlights");

            migrationBuilder.DropColumn(
                name: "FtlDistance",
                table: "FLIGHTSFlights");

            migrationBuilder.DropColumn(
                name: "IsAborted",
                table: "FLIGHTSFlights");

            migrationBuilder.DropColumn(
                name: "StlDistance",
                table: "FLIGHTSFlights");

            migrationBuilder.RenameColumn(
                name: "Origin",
                table: "FLIGHTSFlightSegments",
                newName: "OriginName");

            migrationBuilder.RenameColumn(
                name: "Destination",
                table: "FLIGHTSFlightSegments",
                newName: "OriginId");

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegmentDestinations",
                columns: table => new
                {
                    FLIGHTSFlightSegmentDestinationId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false),
                    SegmentId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentName = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentNaturalId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegmentDestinations", x => x.FLIGHTSFlightSegmentDestinationId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegmentDestinations_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegmentOrigins",
                columns: table => new
                {
                    FLIGHTSFlightSegmentOriginId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false),
                    SegmentId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentName = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentNaturalId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegmentOrigins", x => x.FLIGHTSFlightSegmentOriginId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegmentOrigins_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegmentDestinations_FLIGHTSFlightSegmentId",
                table: "FLIGHTSFlightSegmentDestinations",
                column: "FLIGHTSFlightSegmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegmentOrigins_FLIGHTSFlightSegmentId",
                table: "FLIGHTSFlightSegmentOrigins",
                column: "FLIGHTSFlightSegmentId",
                unique: true);
        }
    }
}
