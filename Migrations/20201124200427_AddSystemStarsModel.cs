﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class AddSystemStarsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SystemStarsModels",
                columns: table => new
                {
                    SystemStarsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    NaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    PositionX = table.Column<double>(type: "REAL", nullable: false),
                    PositionY = table.Column<double>(type: "REAL", nullable: false),
                    PositionZ = table.Column<double>(type: "REAL", nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", nullable: true),
                    SubSectorId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemStarsModels", x => x.SystemStarsModelId);
                });

            migrationBuilder.CreateTable(
                name: "SystemConnections",
                columns: table => new
                {
                    SystemConnectionId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Connection = table.Column<string>(type: "TEXT", nullable: true),
                    SystemStarsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemConnections", x => x.SystemConnectionId);
                    table.ForeignKey(
                        name: "FK_SystemConnections_SystemStarsModels_SystemStarsModelId",
                        column: x => x.SystemStarsModelId,
                        principalTable: "SystemStarsModels",
                        principalColumn: "SystemStarsModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SystemConnections_SystemStarsModelId",
                table: "SystemConnections",
                column: "SystemStarsModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SystemConnections");

            migrationBuilder.DropTable(
                name: "SystemStarsModels");
        }
    }
}
