﻿using Microsoft.EntityFrameworkCore;

using FIORest.Database.Models;

namespace FIORest.Database
{
    public class PRUNDataContext : DbContext
    {
        public DbSet<AuthenticationModel> AuthenticationModels { get; set; }
        public DbSet<PermissionAllowance> PermissionAllowances { get; set; }

        public DbSet<BUIModel> BUIModels { get; set; }
        private DbSet<BUIBuildingCost> BUIBuildingCosts { get; set; }
        private DbSet<BUIRecipeInput> BUIRecipeInputs { get; set; }
        private DbSet<BUIRecipeOutput> BUIRecipeOutputs { get; set; }
        private DbSet<BUIRecipe> BUIRecipes { get; set; }

        public DbSet<CompanyDataModel> CompanyDataModels { get; set; }
        private DbSet<CompanyDataCurrencyBalance> CompanyDataCurrencyBalances { get; set; }

        public DbSet<CXDataModel> CXDataModels { get; set; }
        private DbSet<CXBuyOrder> CXBuyOrders { get; set; }
        private DbSet<CXSellOrder> CXSellOrders { get; set; }

        public DbSet<ExpertsModel> ExpertModels { get; set; }

        public DbSet<InfrastructureModel> InfrastructureModels { get; set; }
        private DbSet<InfrastructureInfo> InfrastructureInfos { get; set; }

        public DbSet<LocalMarketModel> LocalMarketModels { get; set; }
        public DbSet<ShippingAd> ShippingAds { get; set; }
        public DbSet<BuyingAd> BuyingAds { get; set; }
        public DbSet<SellingAd> SellingAds { get; set; }

        public DbSet<PRODLinesModel> PRODLinesModels { get; set; }
        private DbSet<ProductionLine> ProductionLines { get; set; }
        private DbSet<ProductionLineOrder> ProductionLineOrders { get; set; }
        private DbSet<ProductionLineInput> ProductionLineInputs { get; set; }
        private DbSet<ProductionLineOutput> ProductionLineOutputs { get; set; }

        public DbSet<SHIPSModel> SHIPSModels { get; set; }
        private DbSet<SHIPSShip> SHIPSShips { get; set; }

        public DbSet<FLIGHTSModel> FLIGHTSModels { get; set; }
        private DbSet<FLIGHTSFlight> FLIGHTSFlights { get; set; }
        private DbSet<FLIGHTSFlightSegment> FLIGHTSFlightSegments { get; set; }
        private DbSet<OriginLine> FLIGHTSOriginLines { get; set; }
        private DbSet<DestinationLine> FLIGHTSDestinationLines { get; set; }

        public DbSet<PlanetDataModel> PlanetDataModels { get; set; }
        public DbSet<PlanetDataResource> PlanetDataResources { get; set; }
        public DbSet<PlanetBuildRequirement> PlanetBuildRequirements { get; set; }
        private DbSet<PlanetProductionFee> PlanetProductionFees { get; set; }

        public DbSet<SITESModel> SITESModels { get; set; }
        private DbSet<SITESSite> SITESSites { get; set; }
        private DbSet<SITESBuilding> SITESBuildings { get; set; }
        private DbSet<SITESReclaimableMaterial> SITESReclaimableMaterials { get; set; }
        private DbSet<SITESRepairMaterial> SITESRepairMaterials { get; set; }

        public DbSet<WorkforceModel> WorkforceModels { get; set; }
        private DbSet<WorkforceDescription> WorkforceDescriptions { get; set; }
        private DbSet<WorkforceNeed> WorkforceNeeds { get; set; }

        public DbSet<MATModel> MATModels { get; set; }

        public DbSet<StorageModel> StorageModels { get; set; }
        private DbSet<StorageItem> StorageItems { get; set; }

        public DbSet<SystemStarsModel> SystemStarsModels { get; set; }
        private DbSet<SystemConnection> SystemConnections { get; set; }

        public DbSet<WorldSectorsModel> WorldSectorsModels { get; set; }
        private DbSet<SubSector> SubSectors { get; set; }
        private DbSet<SubSectorVertex> SubSectorVertices { get; set; }

        public DbSet<UserDataModel> UserDataModels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite(Globals.ConnectionString).UseLazyLoadingProxies();
        }
    }
}