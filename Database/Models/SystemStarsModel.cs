﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class SystemStarsModel
    {
        [JsonIgnore]
        public int SystemStarsModelId { get; set; }

        public string SystemId { get; set; }
        public string Name { get; set; }
        public string NaturalId { get; set; }
        public string Type { get; set; }

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        public string SectorId { get; set; }
        public string SubSectorId { get; set; }

        public virtual List<SystemConnection> Connections { get; set; } = new List<SystemConnection>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SystemConnection
    {
        [JsonIgnore]
        public int SystemConnectionId { get; set; }

        public string Connection { get; set; }

        [JsonIgnore]
        public int SystemStarsModelId { get; set; }
        [JsonIgnore]
        public virtual SystemStarsModel SystemStarsModel { get; set; }
    }
}
