﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class PlanetDataModel
    {
        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        public string Id { get; set; }
        public string NaturalId { get; set; }
        public string Name { get; set; }
        public string Namer { get; set; }
        public long NamingDataEpochMs { get; set; }
        public bool Nameable { get; set; }

        public double Gravity { get; set; }
        public double MagneticField { get; set; }
        public double Mass { get; set; }
        public double MassEarth { get; set; }

        public double OrbitSemiMajorAxis { get; set; }
        public double OrbitEccentricity { get; set; }
        public double OrbitInclination { get; set; }
        public double OrbitRightAscension { get; set; }
        public double OrbitPeriapsis { get; set; }
        public int OrbitIndex { get; set; }

        public double Pressure { get; set; }
        public double Radiation { get; set; }
        public double Radius { get; set; }

        public virtual List<PlanetDataResource> Resources { get; set; } = new List<PlanetDataResource>();

        public double Sunlight { get; set; }
        public bool Surface { get; set; }
        public double Temperature { get; set; }
        public double Fertility { get; set; }

        public virtual List<PlanetBuildRequirement> BuildRequirements { get; set; } = new List<PlanetBuildRequirement>();

        public bool HasLocalMarket { get; set; }
        public bool HasChamberOfCommerce { get; set; }
        public bool HasWarehouse { get; set; }
        public bool HasAdministrationCenter { get; set; }

        public string FactionCode { get; set; }
        public string FactionName { get; set; }

        public string GovernorId { get; set; }
        public string GovernorUserName { get; set; }

        public string GovernorCorporationId { get; set; }
        public string GovernorCorporationName { get; set; }
        public string GovernorCorporationCode { get; set; }

        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }

        public string CollectorId { get; set; }
        public string CollectorName { get; set; }
        public string CollectorCode { get; set; }

        public virtual List<PlanetProductionFee> ProductionFees { get; set; } = new List<PlanetProductionFee>();
        public double? BaseLocalMarketFee { get; set; }
        public double? LocalMarketFeeFactor { get; set; }

        public double? WarehouseFee { get; set; }

        public string PopulationId { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class PlanetDataResource
    {
        [JsonIgnore]
        public int PlanetDataResourceId { get; set; }

        public string MaterialId { get; set; }
        public string ResourceType { get; set; }
        public double Factor { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }

    public class PlanetBuildRequirement
    {
        [JsonIgnore]
        public int PlanetBuildRequirementId { get; set; }

        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }

        public int MaterialAmount { get; set; }

        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }

    public class PlanetProductionFee
    {
        [JsonIgnore]
        public int PlanetProductionFeeId { get; set; }

        public string Category { get; set; }
        public double FeeAmount { get; set; }
        public string FeeCurrency { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }
}
