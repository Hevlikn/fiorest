﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class PRODLinesModel
    {
        [JsonIgnore]
        public int PRODLinesModelId { get; set; }

        public string SiteId { get; set; }

        public virtual List<ProductionLine> ProductionLines { get; set; } = new List<ProductionLine>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ProductionLine
    {
        [JsonIgnore]
        public int ProductionLineId { get; set; }

        public string SiteId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string Type { get; set; }
        public int Capacity { get; set; }

        public double Efficiency { get; set; }
        public double Condition { get; set; }

        public virtual List<ProductionLineOrder> Orders { get; set; } = new List<ProductionLineOrder>();

        [JsonIgnore]
        public int PRODLinesModelId { get; set; }
        [JsonIgnore]
        public virtual PRODLinesModel PRODLinesModelModel { get; set; }
    }

    public class ProductionLineOrder
    {
        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }

        public virtual List<ProductionLineInput> Inputs { get; set; } = new List<ProductionLineInput>();
        public virtual List<ProductionLineOutput> Outputs { get; set; } = new List<ProductionLineOutput>();

        public long? CreatedEpochMs { get; set; }
        public long? StartedEpochMs { get; set; }
        public long? CompletionEpochMs { get; set; }
        public long? DurationMs { get; set; }
        public long? LastUpdatedEpochMs { get; set; }
        public double? CompletedPercentage { get; set; }

        public bool IsHalted { get; set; }

        public double ProductionFee { get; set; }
        public string ProductionFeeCurrency { get; set; }

        public string ProductionFeeCollectorId { get; set; }
        public string ProductionFeeCollectorName { get; set; }
        public string ProductionFeeCollectorCode { get; set; }

        [JsonIgnore]
        public int ProductionLineId { get; set; }
        [JsonIgnore]
        public virtual ProductionLine ProductionLine { get; set; }
    }

    public class ProductionLineInput
    {
        [JsonIgnore]
        public int ProductionLineInputId { get; set; }

        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialId { get; set; }
        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }
        [JsonIgnore]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }
    }

    public class ProductionLineOutput
    {
        [JsonIgnore]
        public int ProductionLineOutputId { get; set; }

        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialId { get; set; }
        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }
        [JsonIgnore]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }
    }
}
